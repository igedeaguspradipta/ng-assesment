const plugin = require("tailwindcss/plugin");
const defaultTheme = require("tailwindcss/defaultTheme");
/** @type {import('tailwindcss').Config} */

module.exports = {
	content: ["./src/**/*.{html,ts}"],
	theme: {
		extend: {
			fontFamily: {
				sans: ["Poppins", ...defaultTheme.fontFamily.sans],
			},
			colors: {
				primary: "#DC5F00",
				secondary: "#373A40",
				primaryLight: "#686D76",
				secondaryLight: "#EEEEEE",
				danger: "#C40C0C",
			},
		},
	},
	plugins: [
		plugin(function ({ addUtilities, addComponents, e, config }) {
			addComponents({
				".table-container": {
					"overflow-x": "auto",
				},
				".table": {
					"border-collapse": "collapse",
					width: "100%",
					"table-layout": "auto",
					"td,th": {
						padding: 8,
					},
					th: {
						"text-align": "left",
						"background-color": "#686D76",
						color: "#FFFFFF",
					},
					"tr:nth-child(even)": {
						"background-color": "#EEEEEE",
					},
				},
				".btn": {
					border: "none",
					padding: "8px 16px",
					"min-height": "38px",
					"text-align": "center",
					"text-decoration": "none",
					display: "inline-block",
					"font-size": "14px",
					"font-weight": "600",
					"border-radius": "6px",
					"&.btn-primary": {
						"background-color": "#DC5F00",
						color: "#FFFFFF",
						"&:hover": {
							"background-color": "#DC4000",
						},
					},
					"&.btn-secondary": {
						"background-color": "#686D76",
						color: "#FFFFFF",
						"&:hover": {
							"background-color": "#373A40",
						},
					},
					"&.btn-danger": {
						"background-color": "#C40C0C",
						color: "#FFFFFF",
						"&:hover": {
							"background-color": "#B00000",
						},
					},
					"&.btn-warning": {
						"background-color": "#FFBF00",
						color: "#FFFFFF",
						"&:hover": {
							"background-color": "#FF9A00",
						},
					},
					"&:disabled": {
						"background-color": "#EEEEEE",
						color: "#686D76",
						"&:hover": {
							"background-color": "#EEEEEE",
							color: "#686D76",
						},
					},
				},
				".card": {
					padding: "16px",
					"border-radius": "4px",
					"background-color": "#FFFFFF",
					"box-shadow": "0 1px 2px 0 rgb(0 0 0 / 0.05)",
				},
				".page-title": {
					"font-size": "30px",
					"font-weight": "700",
					"margin-bottom": "20px",
				},
				".form-group": {
					"margin-bottom": "16px",

					".label": {
						"margin-bottom": "4px",
						display: "inline-block",
					},
					".input,.select,.textarea": {
						border: "1px solid #686D76",
						width: "100%",
						display: "block",
						"border-radius": "4px",
						padding: "6px 12px",
						transition: "all 0.5s ease-in-out",
						"&:focus": {
							"outline-color": "#686D76",
						},
						"&.error": {
							"border-color": "#C40C0C",
							"outline-color": "#C40C0C",
						},
						"&:disabled": {
							"border-color": "#686D76",
							"background-color": "#EEEEEE",
							color: "#686D76",
						},
					},
					".error-message": {
						color: "#C40C0C",
						"font-size": "12px",
						display: "block",
					},
				},
				".pagination": {
					display: "flex",
					"flex-direction": "row",
					".page-item": {
						"&:first-child": {
							".page-link": {
								"border-top-left-radius": "4px",
								"border-bottom-left-radius": "4px",
							},
						},
						"&:last-child": {
							".page-link": {
								"border-top-right-radius": "4px",
								"border-bottom-right-radius": "4px",
							},
						},
						"&:not(:first-child)": {
							".page-link": {
								"margin-left": "-1px",
							},
						},
						"&.active": {
							".page-link": {
								"background-color": "#DC5F00",
								"border-color": "#DC5F00",
								color: "#FFFFFF",
							},
						},
						"&.disabled": {
							".page-link": {
								"background-color": "#EEEEEE",
								color: "#686D76",
								cursor: "auto",
							},
						},
					},
					".page-link": {
						display: "block",
						padding: "4px 12px",
						"text-decoration": "none",
						border: "1px solid #EEEEEE",
						transition: "all 0.15s ease-in-out",
						cursor: "pointer",
						"&:hover": {
							"background-color": "#EEEEEE",
						},
					},
				},
				".badge": {
					padding: "4px 8px",
					"border-radius": "6px",
					"background-color": "#686D76",
					color: "#FFFFFF",
					"font-size": "12px",
					"font-weight": "500",
					"&.badge-primary": {
						"background-color": "#DC5F00",
					},
					"&.background-secondary": {
						"background-color": "#373A40",
					},
				},
				".alert": {
					padding: "8px 12px",
					"background-color": "#DC5F00",
					"border-radius": "6px",
					color: "#FFFFFF",
					"&.alert-primary": {
						"background-color": "#DC5F00",
					},
					"&.alert-secondary": {
						"background-color": "#686D76",
					},
					"&.alert-error": {
						"background-color": "#EE4E4E",
					},
				},
			});
		}),
	],
};

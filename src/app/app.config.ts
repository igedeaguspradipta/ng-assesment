import { InMemoryWebApiModule } from "angular-in-memory-web-api";

import { provideHttpClient, withFetch } from "@angular/common/http";
import {
	ApplicationConfig,
	importProvidersFrom,
	provideZoneChangeDetection,
} from "@angular/core";
import { provideClientHydration } from "@angular/platform-browser";
import { provideRouter, withComponentInputBinding } from "@angular/router";

import { routes } from "./app.routes";
import { InMemoryDataService } from "./shared/services/in-memory-data/in-memory-data.service";

export const appConfig: ApplicationConfig = {
	providers: [
		provideZoneChangeDetection({ eventCoalescing: true }),
		provideRouter(routes, withComponentInputBinding()),
		provideClientHydration(),
		provideHttpClient(withFetch()),
		importProvidersFrom(
			InMemoryWebApiModule.forRoot(InMemoryDataService, { delay: 1500 })
		),
	],
};

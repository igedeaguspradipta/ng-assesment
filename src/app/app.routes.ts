import { Routes } from "@angular/router";

import { routes as employeeRoutes } from "./employees/employee.routes";
import { LoginComponent } from "./login/login.component";
import { MainLayoutComponent } from "./main-layout/main-layout.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { authGuardGuard } from "./shared/guards/auth-guard/auth-guard.guard";

export const routes: Routes = [
	{ path: "login", component: LoginComponent },
	{ path: "", redirectTo: "/employee", pathMatch: "full" },
	{
		path: "",
		component: MainLayoutComponent,
		canActivateChild: [authGuardGuard],
		children: [...employeeRoutes],
	},
	{ path: "**", component: PageNotFoundComponent },
];

import { Component } from "@angular/core";
import {
	Router,
	RouterLink,
	RouterLinkActive,
	RouterOutlet,
} from "@angular/router";

import { AuthService } from "../shared/services/auth/auth.service";

@Component({
	selector: "app-main-layout",
	standalone: true,
	imports: [RouterOutlet, RouterLinkActive, RouterLink],
	templateUrl: "./main-layout.component.html",
	styleUrl: "./main-layout.component.scss",
})
export class MainLayoutComponent {
	constructor(private authService: AuthService, private router: Router) {}

	logout() {
		this.authService.logout();
		this.router.navigate(["/login"]);
	}
}

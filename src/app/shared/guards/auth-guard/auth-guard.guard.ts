import { inject } from "@angular/core";
import { CanActivateChildFn, CanActivateFn, Router } from "@angular/router";

import { AuthService } from "../../services/auth/auth.service";

export const authGuardGuard: CanActivateChildFn = (route, state) => {
	const authService = inject(AuthService);
	const router = inject(Router);

	if (!authService.isLoggedIn) {
		return router.navigate(["login"]);
	}
	return true;
};

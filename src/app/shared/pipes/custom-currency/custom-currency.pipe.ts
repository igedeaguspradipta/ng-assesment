import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
	name: "customCurrency",
	standalone: true,
})
export class CustomCurrencyPipe implements PipeTransform {
	transform(value: number): string {
		const res = new Intl.NumberFormat("id-ID", {
			style: "currency",
			currency: "IDR",
			currencySign: "accounting",
		}).format(value);
		return res.replace(/(Rp\s)/g, "Rp. ");
	}
}

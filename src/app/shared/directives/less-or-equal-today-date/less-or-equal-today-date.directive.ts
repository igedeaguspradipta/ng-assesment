import { Directive } from "@angular/core";
import {
	AbstractControl,
	NG_VALIDATORS,
	ValidationErrors,
	Validator,
	ValidatorFn,
} from "@angular/forms";

@Directive({
	selector: "[appLessOrEqualTodayDate]",
	providers: [
		{
			provide: NG_VALIDATORS,
			useExisting: LessOrEqualTodayDateDirective,
			multi: true,
		},
	],
	standalone: true,
})
export class LessOrEqualTodayDateDirective implements Validator {
	validate(control: AbstractControl<any, any>): ValidationErrors | null {
		return lessOrEqualTodayDateValidator()(control);
	}
}

export function lessOrEqualTodayDateValidator(): ValidatorFn {
	return (control: AbstractControl): ValidationErrors | null => {
		const today = new Date();
		today.setHours(0, 0, 0, 0);

		const val = control.value;
		const selectedDate = new Date(val);

		if (selectedDate.getTime() > today.getTime()) {
			return { lessOrEqualTodayDate: true };
		}
		return null;
	};
}

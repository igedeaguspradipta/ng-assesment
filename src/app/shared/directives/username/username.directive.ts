import { Directive } from "@angular/core";
import {
	AbstractControl,
	NG_VALIDATORS,
	ValidationErrors,
	Validator,
	ValidatorFn,
} from "@angular/forms";

import { LessOrEqualTodayDateDirective } from "../less-or-equal-today-date/less-or-equal-today-date.directive";

@Directive({
	selector: "[appUsername]",
	providers: [
		{
			provide: NG_VALIDATORS,
			useExisting: UsernameDirective,
			multi: true,
		},
	],
	standalone: true,
})
export class UsernameDirective implements Validator {
	constructor() {}
	validate(control: AbstractControl<any, any>): ValidationErrors | null {
		return usernameValidator()(control);
	}
}

export function usernameValidator(): ValidatorFn {
	return (control: AbstractControl): ValidationErrors | null => {
		if (control.value.match(/[ ]/g)) {
			return { username: true };
		}
		return null;
	};
}

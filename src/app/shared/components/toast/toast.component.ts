import {Component, EventEmitter, Output} from '@angular/core';

@Component({
	selector: "app-toast",
	standalone: true,
	imports: [],
	templateUrl: "./toast.component.html",
	styleUrl: "./toast.component.scss",
})
export class ToastComponent {
	@Output() close = new EventEmitter();

	handleClose() {
		this.close.emit();
	}
}

import { Injectable } from "@angular/core";

@Injectable({
	providedIn: "root",
})
export class AuthService {
	constructor() {}

	login(username: string, password: string) {
		try {
			if (username === "admin" && password === "admin") {
				localStorage.setItem("loggedin", "true");
				return true;
			}
			return false;
		} catch (error) {
			console.log(error);
			return false;
		}
	}

	get isLoggedIn() {
		try {
			return localStorage.getItem("loggedin") === "true";
		} catch (error) {
			return false;
		}
	}

	logout() {
		localStorage.removeItem("loggedin");
	}
}

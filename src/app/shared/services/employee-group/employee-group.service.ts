import { catchError, Observable, tap, throwError } from "rxjs";

import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { EmployeeGroup } from "../../interfaces/employee-group";

@Injectable({
	providedIn: "root",
})
export class EmployeeGroupService {
	private employeeGroupUrl = "api/employeeGroups";

	constructor(private httpClient: HttpClient) {}

	getEmployeeGroups(): Observable<EmployeeGroup[]> {
		return this.httpClient.get<EmployeeGroup[]>(this.employeeGroupUrl).pipe(
			tap((_) => console.log("fetched employee groups")),
			catchError((error: HttpErrorResponse) => {
				console.error(error);
				return throwError(() => error);
			})
		);
	}
}

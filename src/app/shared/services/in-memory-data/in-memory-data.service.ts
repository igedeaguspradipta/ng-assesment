import { InMemoryDbService } from "angular-in-memory-web-api";

import { Injectable } from "@angular/core";

import employeeData from "../../../../assets/data/employee.json";
import employeeGroupData from "../../../../assets/data/group.json";
import { Employee } from "../../interfaces/employee";
import { EmployeeGroup } from "../../interfaces/employee-group";

@Injectable({
	providedIn: "root",
})
export class InMemoryDataService implements InMemoryDbService {
	constructor() {}

	createDb() {
		const employees: Employee[] = employeeData.map((employee, index) => ({
			...employee,
			id: index + 1,
			birthDate: new Date(employee.birthDate),
			description: new Date(employee.description),
		}));

		const employeeGroups: EmployeeGroup[] = employeeGroupData.map(
			(item, index) => ({
				id: index + 1,
				name: item,
			})
		);

		return {
			employees,
			employeeGroups,
		};
	}
}

export type ISortBy = "username" | "firstName" | "lastName" | "status";

export type ISortDirection = "asc" | "desc";

export interface GetEmployee {
	username?: string | null;
	firstName?: string | null;
	lastName?: string | null;
	email?: string | null;
	perPage?: number;
	page?: number;
	sortBy?: ISortBy;
	sortDirection?: ISortDirection;
}

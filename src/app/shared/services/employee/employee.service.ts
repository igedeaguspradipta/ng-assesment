import {catchError, map, Observable, tap, throwError} from 'rxjs';

import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';

import {Employee} from '../../interfaces/employee';
import {GetEmployee} from './employee';

@Injectable({
	providedIn: "root",
})
export class EmployeeService {
	private employeesUrl = "api/employees";

	list: Employee[] = [];

	constructor(private httpClient: HttpClient) {}

	getEmployees(arg?: GetEmployee): Observable<Employee[]> {
		let params: string[] = [];
		if (arg) {
			for (let key of Object.keys(arg)) {
				params.push(`${key}=${(arg as Record<string, any>)[key]}`);
			}
		}
		const queryParams = params.join("&");

		return this.httpClient.get<Employee[]>(this.employeesUrl).pipe(
			tap((_) => console.log(`fetched employees with arg=${queryParams}`)),
			map((employees) => {
				let list = employees.filter((item) => {
					if (arg?.username || arg?.firstName || arg?.lastName || arg?.email) {
						const matchUsername = arg.username
							? new RegExp(arg.username).test(item.username)
							: false;
						const matchFirstName = arg.firstName
							? new RegExp(arg.firstName).test(item.firstName)
							: false;
						const matchLastName = arg.lastName
							? new RegExp(arg.lastName).test(item.lastName)
							: false;
						const matchEmail = arg.email
							? new RegExp(arg.email, "g").test(item.email)
							: false;
						if (
							matchUsername ||
							matchFirstName ||
							matchLastName ||
							matchEmail
						) {
							return item;
						}
						return null;
					}
					return item;
				});

				if (arg?.sortBy && arg.sortDirection) {
					if (arg.sortBy === "username") {
						if (arg.sortDirection === "asc") {
							list = list.sort((a, b) => {
								if (a.username < b.username) {
									return -1;
								} else if (a.username > b.username) {
									return 1;
								}
								return 0;
							});
						} else if (arg.sortDirection === "desc") {
							list = list.sort((a, b) => {
								if (a.username > b.username) {
									return -1;
								} else if (a.username < b.username) {
									return 1;
								}
								return 0;
							});
						}
					} else if (arg.sortBy === "firstName") {
						if (arg.sortDirection === "asc") {
							list = list.sort((a, b) => {
								if (a.firstName < b.firstName) {
									return -1;
								} else if (a.firstName > b.firstName) {
									return 1;
								}
								return 0;
							});
						} else if (arg.sortDirection === "desc") {
							list = list.sort((a, b) => {
								if (a.firstName > b.firstName) {
									return -1;
								} else if (a.firstName < b.firstName) {
									return 1;
								}
								return 0;
							});
						}
					} else if (arg.sortBy === "lastName") {
						if (arg.sortDirection === "asc") {
							list = list.sort((a, b) => {
								if (a.lastName < b.lastName) {
									return -1;
								} else if (a.lastName > b.lastName) {
									return 1;
								}
								return 0;
							});
						} else if (arg.sortDirection === "desc") {
							list = list.sort((a, b) => {
								if (a.lastName > b.lastName) {
									return -1;
								} else if (a.lastName < b.lastName) {
									return 1;
								}
								return 0;
							});
						}
					} else if (arg.sortBy === "status") {
						if (arg.sortDirection === "asc") {
							list = list.sort((a, b) => {
								if (a.status < b.status) {
									return -1;
								} else if (a.status > b.status) {
									return 1;
								}
								return 0;
							});
						} else if (arg.sortDirection === "desc") {
							list = list.sort((a, b) => {
								if (a.status > b.status) {
									return -1;
								} else if (a.status < b.status) {
									return 1;
								}
								return 0;
							});
						}
					}
				}

				return list;
			}),
			catchError((error: HttpErrorResponse) => {
				console.error(error);
				return throwError(() => error);
			})
		);
	}

	getEmployee(id: number): Observable<Employee> {
		return this.httpClient.get<Employee>(`${this.employeesUrl}/${id}`).pipe(
			tap((_) => console.log("fetched employee")),
			catchError((error: HttpErrorResponse) => {
				console.error(error);
				return throwError(() => error);
			})
		);
	}

	storeEmployee(employee: Employee): Observable<Employee> {
		return this.httpClient.post<Employee>(this.employeesUrl, employee).pipe(
			tap((newEmployee) =>
				console.log(`stored employee with id=${newEmployee.id}`)
			),
			catchError((error: HttpErrorResponse) => {
				console.error(error);
				return throwError(() => error);
			})
		);
	}

	updateEmployee(employee: Employee): Observable<Employee> {
		return this.httpClient.put<Employee>(this.employeesUrl, employee).pipe(
			tap((_) => console.log(`updated employee with id=${employee.id}`)),
			catchError((error: HttpErrorResponse) => {
				console.error(error);
				return throwError(() => error);
			})
		);
	}

	deleteEmployee(id: number): Observable<Employee> {
		const url = `${this.employeesUrl}/${id}`;
		return this.httpClient.delete<Employee>(url).pipe(
			tap(() => console.log(`updated employee with id=${id}`)),
			catchError((error: HttpErrorResponse) => {
				console.error(error);
				return throwError(() => error);
			})
		);
	}
}

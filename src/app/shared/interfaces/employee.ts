export interface Employee {
	id: number;
	firstName: string;
	lastName: string;
	username: string;
	email: string;
	birthDate: Date;
	basicSalary: number;
	status: string;
	group: string;
	description: Date;
}

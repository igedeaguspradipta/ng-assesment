import { NgIf } from "@angular/common";
import { Component } from "@angular/core";
import {
	FormControl,
	FormGroup,
	ReactiveFormsModule,
	Validators,
} from "@angular/forms";
import { Router } from "@angular/router";

import {
	UsernameDirective,
	usernameValidator,
} from "../shared/directives/username/username.directive";
import { AuthService } from "../shared/services/auth/auth.service";

@Component({
	selector: "app-login",
	standalone: true,
	imports: [ReactiveFormsModule, NgIf, UsernameDirective],
	templateUrl: "./login.component.html",
	styleUrl: "./login.component.scss",
})
export class LoginComponent {
	errorMessage: string = "";

	form = new FormGroup({
		username: new FormControl("", [
			Validators.required,
			Validators.minLength(4),
			usernameValidator(),
		]),
		password: new FormControl("", [Validators.required]),
	});

	constructor(private authService: AuthService, private router: Router) {}

	get username() {
		return this.form.get("username");
	}

	get password() {
		return this.form.get("password");
	}

	onSubmit() {
		const username = this.username?.value;
		const password = this.password?.value;

		if (!username || !password) {
			return;
		}

		const res = this.authService.login(username, password);
		console.log(res);

		if (!res) {
			this.errorMessage = "Invalid username or password";
			return;
		}

		this.router.navigate([""]);
	}
}

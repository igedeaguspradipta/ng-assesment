import dayjs from 'dayjs';

import {Location, NgFor, NgIf} from '@angular/common';
import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

import {ToastComponent} from '../../shared/components/toast/toast.component';
import {
    LessOrEqualTodayDateDirective, lessOrEqualTodayDateValidator
} from '../../shared/directives/less-or-equal-today-date/less-or-equal-today-date.directive';
import {
    UsernameDirective, usernameValidator
} from '../../shared/directives/username/username.directive';
import {Employee} from '../../shared/interfaces/employee';
import {EmployeeGroup} from '../../shared/interfaces/employee-group';
import {EmployeeGroupService} from '../../shared/services/employee-group/employee-group.service';
import {EmployeeService} from '../../shared/services/employee/employee.service';

@Component({
	selector: "app-employee-editor",
	standalone: true,
	imports: [
		ReactiveFormsModule,
		NgIf,
		NgFor,
		LessOrEqualTodayDateDirective,
		UsernameDirective,
		ToastComponent,
	],
	templateUrl: "./employee-editor.component.html",
	styleUrl: "./employee-editor.component.scss",
})
export class EmployeeEditorComponent implements OnInit {
	id?: number = undefined;

	form = new FormGroup({
		username: new FormControl({ value: "", disabled: true }, [
			Validators.required,
			Validators.minLength(4),
			usernameValidator(),
		]),
		firstName: new FormControl({ value: "", disabled: true }, [
			Validators.required,
		]),
		lastName: new FormControl({ value: "", disabled: true }, [
			Validators.required,
		]),
		email: new FormControl({ value: "", disabled: true }, [
			Validators.required,
			Validators.email,
		]),
		birthDate: new FormControl({ value: "", disabled: true }, [
			Validators.required,
			lessOrEqualTodayDateValidator(),
		]),
		basicSalary: new FormControl({ value: "", disabled: true }, [
			Validators.required,
		]),
		status: new FormControl({ value: "", disabled: true }, [
			Validators.required,
		]),
		group: new FormControl({ value: "", disabled: true }, [
			Validators.required,
		]),
		description: new FormControl({ value: "", disabled: true }, [
			Validators.required,
		]),
	});

	employeeGroupList: EmployeeGroup[] = [];

	isShowToast: boolean = false;

	toastMessage: string = "";

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private location: Location,
		private employeeGroupService: EmployeeGroupService,
		private employeeService: EmployeeService
	) {}

	ngOnInit(): void {
		const id = this.route.snapshot.paramMap.get("id");
		if (!id) {
			return;
		}
		this.id = Number(id);
		this.employeeService.getEmployee(Number(id)).subscribe({
			next: (employee) => {
				this.form.setValue({
					username: employee.username || "",
					firstName: employee.firstName || "",
					lastName: employee.lastName || "",
					email: employee.email || "",
					birthDate: employee.birthDate
						? dayjs(employee.birthDate).format("YYYY-MM-DD")
						: "",
					basicSalary: employee.basicSalary
						? employee.basicSalary.toString()
						: "",
					group: employee.group || "",
					status: employee.status || "",
					description: employee.description
						? dayjs(employee.description).format("YYYY-MM-DD")
						: "",
				});

				this.username?.enable();
				this.firstName?.enable();
				this.lastName?.enable();
				this.email?.enable();
				this.birthDate?.enable();
				this.basicSalary?.enable();
				this.status?.enable();
				this.description?.enable();
			},
		});

		this.employeeGroupService.getEmployeeGroups().subscribe({
			next: (employeeGroups) => {
				this.employeeGroupList = employeeGroups;
				this.group?.enable();
			},
		});
	}

	get username() {
		return this.form.get("username");
	}

	get firstName() {
		return this.form.get("firstName");
	}

	get lastName() {
		return this.form.get("lastName");
	}

	get email() {
		return this.form.get("email");
	}

	get birthDate() {
		return this.form.get("birthDate");
	}

	get basicSalary() {
		return this.form.get("basicSalary");
	}

	get status() {
		return this.form.get("status");
	}

	get group() {
		return this.form.get("group");
	}

	get description() {
		return this.form.get("description");
	}

	back() {
		this.location.back();
	}

	onSubmit() {
		const username = this.username?.value;
		const firstName = this.firstName?.value;
		const lastName = this.lastName?.value;
		const email = this.email?.value;
		const birthDate = this.birthDate?.value;
		const basicSalary = this.basicSalary?.value;
		const status = this.status?.value;
		const group = this.group?.value;
		const description = this.group?.value;

		if (
			!username ||
			!firstName ||
			!lastName ||
			!email ||
			!birthDate ||
			!basicSalary ||
			!status ||
			!group ||
			!description ||
			!this.id
		) {
			return;
		}

		const data: Employee = {
			id: this.id,
			username,
			firstName,
			lastName,
			email,
			birthDate: new Date(birthDate),
			basicSalary: Number(basicSalary),
			status,
			group,
			description: new Date(description),
		};
		this.employeeService.updateEmployee(data).subscribe({
			next: () => {
				this.isShowToast = true;
				this.toastMessage = "Employee updated";
				setTimeout(() => {
					this.router.navigate(["employee"]);
				}, 1500);
			},
		});
	}

	toggleToast() {
		this.isShowToast = !this.isShowToast;
		this.router.navigate(["employee"]);
	}
}

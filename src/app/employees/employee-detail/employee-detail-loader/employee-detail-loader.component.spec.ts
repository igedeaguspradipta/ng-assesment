import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeDetailLoaderComponent } from './employee-detail-loader.component';

describe('EmployeeDetailLoaderComponent', () => {
  let component: EmployeeDetailLoaderComponent;
  let fixture: ComponentFixture<EmployeeDetailLoaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [EmployeeDetailLoaderComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EmployeeDetailLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

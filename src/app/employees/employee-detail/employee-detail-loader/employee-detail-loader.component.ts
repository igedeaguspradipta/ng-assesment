import { Component } from '@angular/core';

@Component({
  selector: 'app-employee-detail-loader',
  standalone: true,
  imports: [],
  templateUrl: './employee-detail-loader.component.html',
  styleUrl: './employee-detail-loader.component.scss'
})
export class EmployeeDetailLoaderComponent {

}

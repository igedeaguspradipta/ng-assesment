import {DatePipe, Location, NgIf} from '@angular/common';
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NotFoundComponent} from '../../shared/components/not-found/not-found.component';
import {Employee} from '../../shared/interfaces/employee';
import {CustomCurrencyPipe} from '../../shared/pipes/custom-currency/custom-currency.pipe';
import {EmployeeService} from '../../shared/services/employee/employee.service';
import {
    EmployeeDetailLoaderComponent
} from './employee-detail-loader/employee-detail-loader.component';

@Component({
	selector: "app-employee-detail",
	standalone: true,
	imports: [
		EmployeeDetailLoaderComponent,
		NgIf,
		NotFoundComponent,
		DatePipe,
		CustomCurrencyPipe,
	],
	templateUrl: "./employee-detail.component.html",
	styleUrl: "./employee-detail.component.scss",
})
export class EmployeeDetailComponent implements OnInit {
	data?: Employee;

	isLoading: boolean = true;

	constructor(
		private route: ActivatedRoute,
		private location: Location,
		private employeeService: EmployeeService
	) {}

	ngOnInit(): void {
		const id = this.route.snapshot.paramMap.get("id");
		if (!id) {
			return;
		}

		this.isLoading = true;
		this.employeeService.getEmployee(Number(id)).subscribe({
			next: (employee) => {
				this.data = employee;
				this.isLoading = false;
			},
			error: () => {
				this.isLoading = false;
			},
		});
	}

	back() {
		this.location.back();
	}
}

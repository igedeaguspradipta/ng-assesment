import {DatePipe, Location, NgIf} from '@angular/common';
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {NotFoundComponent} from '../../shared/components/not-found/not-found.component';
import {ToastComponent} from '../../shared/components/toast/toast.component';
import {Employee} from '../../shared/interfaces/employee';
import {CustomCurrencyPipe} from '../../shared/pipes/custom-currency/custom-currency.pipe';
import {EmployeeService} from '../../shared/services/employee/employee.service';
import {
    EmployeeDetailLoaderComponent
} from '../employee-detail/employee-detail-loader/employee-detail-loader.component';

@Component({
	selector: "app-employee-destroyer",
	standalone: true,
	imports: [
		EmployeeDetailLoaderComponent,
		NgIf,
		NotFoundComponent,
		DatePipe,
		CustomCurrencyPipe,
		ToastComponent,
	],
	templateUrl: "./employee-destroyer.component.html",
	styleUrl: "./employee-destroyer.component.scss",
})
export class EmployeeDestroyerComponent implements OnInit {
	data?: Employee;

	isLoading: boolean = true;

	isShowToast: boolean = false;

	toastMessage: string = "";

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private location: Location,
		private employeeService: EmployeeService
	) {}

	ngOnInit(): void {
		const id = this.route.snapshot.paramMap.get("id");
		if (!id) {
			return;
		}

		this.isLoading = true;
		this.employeeService.getEmployee(Number(id)).subscribe({
			next: (employee) => {
				console.log(employee);
				this.data = employee;
				this.isLoading = false;
			},
			error: () => {
				this.isLoading = false;
			},
		});
	}

	back() {
		this.location.back();
	}

	delete() {
		if (!this.data?.id) {
			return;
		}
		console.log("id", this.data.id);
		this.employeeService.deleteEmployee(this.data?.id).subscribe({
			next: (employee) => {
				this.isShowToast = true;
				this.toastMessage = "Employee deleted";
				setTimeout(() => {
					this.router.navigate(["employee"]);
				}, 1500);
			},
		});
	}

	toggleToast() {
		this.isShowToast = !this.isShowToast;
		this.router.navigate(["employee"]);
	}
}

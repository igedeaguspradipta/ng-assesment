import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeDestroyerComponent } from './employee-destroyer.component';

describe('EmployeeDestroyerComponent', () => {
  let component: EmployeeDestroyerComponent;
  let fixture: ComponentFixture<EmployeeDestroyerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [EmployeeDestroyerComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EmployeeDestroyerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

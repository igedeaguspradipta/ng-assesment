import {map, tap} from 'rxjs';

import {NgFor, NgIf} from '@angular/common';
import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators} from '@angular/forms';
import {ActivatedRoute, Router, RouterLink} from '@angular/router';

import {
    UsernameDirective, usernameValidator
} from '../../shared/directives/username/username.directive';
import {Employee} from '../../shared/interfaces/employee';
import {GetEmployee, ISortBy, ISortDirection} from '../../shared/services/employee/employee';
import {EmployeeService} from '../../shared/services/employee/employee.service';
import {EmployeeListLoaderComponent} from './employee-list-loader/employee-list-loader.component';

@Component({
	selector: "app-employee-list",
	standalone: true,
	imports: [
		RouterLink,
		NgIf,
		NgFor,
		ReactiveFormsModule,
		UsernameDirective,
		FormsModule,
		EmployeeListLoaderComponent,
	],
	templateUrl: "./employee-list.component.html",
	styleUrl: "./employee-list.component.scss",
})
export class EmployeeListComponent implements OnInit {
	list: Employee[] = [];

	totalPage: number = 1;

	pageSizeOptions: number[] = [10, 25, 50, 100];

	sortBy?: ISortBy = "username";

	sortDirection?: ISortDirection = "asc";

	perPage: number = this.pageSizeOptions[0];

	page: number = 1;

	isLoading: boolean = true;

	searchForm = new FormGroup({
		username: new FormControl("", [
			Validators.minLength(4),
			usernameValidator(),
		]),
		firstName: new FormControl(""),
		lastName: new FormControl(""),
		email: new FormControl("", [Validators.email]),
	});

	constructor(
		private employeeService: EmployeeService,
		private router: Router,
		private route: ActivatedRoute
	) {}

	ngOnInit(): void {
		this.route.queryParams.subscribe((param) => {
			const objectKeys = Object.keys(param);
			if (objectKeys.includes("username")) {
				this.searchForm.patchValue({ username: param["username"] });
			}
			if (objectKeys.includes("firstName")) {
				this.searchForm.patchValue({ firstName: param["firstName"] });
			}
			if (objectKeys.includes("lastName")) {
				this.searchForm.patchValue({ lastName: param["lastName"] });
			}
			if (objectKeys.includes("email")) {
				this.searchForm.patchValue({ email: param["email"] });
			}
			if (objectKeys.includes("perPage")) {
				this.perPage = Number(param["perPage"]);
			}
			if (objectKeys.includes("page")) {
				this.page = Number(param["page"]);
			}
		});

		this.getList({
			page: this.page,
			perPage: this.perPage,
			sortBy: this.sortBy,
			sortDirection: this.sortDirection,
		});
	}

	get username() {
		return this.searchForm.get("username");
	}

	get email() {
		return this.searchForm.get("email");
	}

	get firstName() {
		return this.searchForm.get("firstName");
	}

	get lastName() {
		return this.searchForm.get("lastName");
	}

	get paginationCounter() {
		return new Array(this.totalPage);
	}

	isPageActive(page: number) {
		return this.page === page;
	}

	getList(param: GetEmployee) {
		this.isLoading = true;
		const perPage = param.perPage || this.pageSizeOptions[0];
		const page = param.page || 1;

		const offset = perPage * (page - 1);
		const endOffset = offset + this.perPage;

		this.employeeService
			.getEmployees(param)
			.pipe(
				tap(
					(employees) =>
						(this.totalPage = Math.floor(employees.length / perPage))
				),
				map((employees) => employees.slice(offset, endOffset))
			)
			.subscribe({
				next: (heroes) => {
					this.list = heroes;
					this.isLoading = false;
				},
				error: (_) => {
					this.isLoading = false;
				},
			});

		this.page = param.page || 1;

		this.router.navigate(["/employee"], {
			relativeTo: this.route,
			queryParams: param,
		});
	}

	onSubmitSearch() {
		this.getList({
			page: 1,
			perPage: this.perPage,
			sortBy: this.sortBy,
			sortDirection: this.sortDirection,
			username: this.username?.value,
			firstName: this.firstName?.value,
			lastName: this.lastName?.value,
			email: this.email?.value,
		});
	}

	onReset() {
		const arg: GetEmployee = {
			username: "",
			firstName: "",
			lastName: "",
			email: "",
			perPage: this.pageSizeOptions[0],
			page: 1,
			sortBy: "username",
			sortDirection: "asc",
		};
		this.getList(arg);
		this.router.navigate(["/employee"], {
			relativeTo: this.route,
			queryParams: arg,
		});
	}

	onPrevious() {
		let page = this.page;
		if (page > 1) {
			page = page - 1;
		}
		this.getList({
			page,
			perPage: this.perPage,
			sortBy: this.sortBy,
			sortDirection: this.sortDirection,
			username: this.username?.value,
			firstName: this.firstName?.value,
			lastName: this.lastName?.value,
			email: this.email?.value,
		});
	}

	onNext() {
		let page = this.page;
		if (page < this.totalPage) {
			page = page + 1;
		}
		this.getList({
			page,
			perPage: this.perPage,
			sortBy: this.sortBy,
			sortDirection: this.sortDirection,
			username: this.username?.value,
			firstName: this.firstName?.value,
			lastName: this.lastName?.value,
			email: this.email?.value,
		});
	}

	getItemNumber(index: number) {
		return this.perPage * (this.page - 1) + index + 1;
	}

	onSortBy(sortBy: ISortBy) {
		let sortDirection = this.sortDirection;

		if (sortBy !== this.sortBy) {
			sortDirection = "asc";
		} else if (!sortDirection) {
			sortDirection = "asc";
		} else if (sortDirection === "asc") {
			sortDirection = "desc";
		} else if (sortDirection === "desc") {
			sortDirection = undefined;
		}

		this.sortBy = sortBy;
		this.sortDirection = sortDirection;

		this.getList({
			page: 1,
			perPage: this.perPage,
			sortBy: sortBy,
			sortDirection,
			username: this.username?.value,
			firstName: this.firstName?.value,
			lastName: this.lastName?.value,
			email: this.email?.value,
		});
	}

	onPerPageChange(value: string) {
		const perPage = Number(value);
		this.perPage = perPage;
		this.getList({
			perPage,
			page: 1,
			sortBy: this.sortBy,
			sortDirection: this.sortDirection,
			username: this.username?.value,
			firstName: this.firstName?.value,
			lastName: this.lastName?.value,
			email: this.email?.value,
		});
	}
}

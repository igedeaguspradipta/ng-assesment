import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeListLoaderComponent } from './employee-list-loader.component';

describe('EmployeeListLoaderComponent', () => {
  let component: EmployeeListLoaderComponent;
  let fixture: ComponentFixture<EmployeeListLoaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [EmployeeListLoaderComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EmployeeListLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

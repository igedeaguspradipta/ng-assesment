import { NgFor } from "@angular/common";
import { Component } from "@angular/core";

@Component({
	selector: "[loader]",
	standalone: true,
	imports: [NgFor],
	templateUrl: "./employee-list-loader.component.html",
	styleUrl: "./employee-list-loader.component.scss",
})
export class EmployeeListLoaderComponent {
	get loaderCounter() {
		return new Array(10);
	}
}

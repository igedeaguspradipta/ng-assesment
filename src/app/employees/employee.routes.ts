import {Routes} from '@angular/router';

import {EmployeeCreatorComponent} from './employee-creator/employee-creator.component';
import {EmployeeDestroyerComponent} from './employee-destroyer/employee-destroyer.component';
import {EmployeeDetailComponent} from './employee-detail/employee-detail.component';
import {EmployeeEditorComponent} from './employee-editor/employee-editor.component';
import {EmployeeListComponent} from './employee-list/employee-list.component';

export const routes: Routes = [
	{ path: "employee/:id/edit", component: EmployeeEditorComponent },
	{ path: "employee/:id/delete", component: EmployeeDestroyerComponent },
	{ path: "employee/new", component: EmployeeCreatorComponent },
	{ path: "employee/:id", component: EmployeeDetailComponent },
	{ path: "employee", component: EmployeeListComponent },
];

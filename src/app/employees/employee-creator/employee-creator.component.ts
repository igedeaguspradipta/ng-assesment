import {Location, NgFor, NgIf} from '@angular/common';
import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from '@angular/forms';
import {Router} from '@angular/router';

import {ToastComponent} from '../../shared/components/toast/toast.component';
import {
    LessOrEqualTodayDateDirective, lessOrEqualTodayDateValidator
} from '../../shared/directives/less-or-equal-today-date/less-or-equal-today-date.directive';
import {
    UsernameDirective, usernameValidator
} from '../../shared/directives/username/username.directive';
import {Employee} from '../../shared/interfaces/employee';
import {EmployeeGroup} from '../../shared/interfaces/employee-group';
import {EmployeeGroupService} from '../../shared/services/employee-group/employee-group.service';
import {EmployeeService} from '../../shared/services/employee/employee.service';

@Component({
	selector: "app-employee-creator",
	standalone: true,
	imports: [
		ReactiveFormsModule,
		NgIf,
		NgFor,
		LessOrEqualTodayDateDirective,
		UsernameDirective,
		ToastComponent,
	],
	templateUrl: "./employee-creator.component.html",
	styleUrl: "./employee-creator.component.scss",
})
export class EmployeeCreatorComponent implements OnInit {
	form = new FormGroup({
		username: new FormControl("", [
			Validators.required,
			Validators.minLength(4),
			usernameValidator(),
		]),
		firstName: new FormControl("", [Validators.required]),
		lastName: new FormControl("", [Validators.required]),
		email: new FormControl("", [Validators.required, Validators.email]),
		birthDate: new FormControl("", [
			Validators.required,
			lessOrEqualTodayDateValidator(),
		]),
		basicSalary: new FormControl("", [Validators.required]),
		status: new FormControl("", [Validators.required]),
		group: new FormControl({ value: "", disabled: true }, [
			Validators.required,
		]),
		description: new FormControl("", [Validators.required]),
	});

	employeeGroupList: EmployeeGroup[] = [];

	isShowToast: boolean = false;

	toastMessage: string = "";

	constructor(
		private router: Router,
		private location: Location,
		private employeeGroupService: EmployeeGroupService,
		private employeeService: EmployeeService
	) {}

	ngOnInit(): void {
		this.employeeGroupService.getEmployeeGroups().subscribe({
			next: (employeeGroups) => {
				this.employeeGroupList = employeeGroups;
				if (employeeGroups.length > 0) {
					this.group?.enable();
				}
			},
		});
	}

	get username() {
		return this.form.get("username");
	}

	get firstName() {
		return this.form.get("firstName");
	}

	get lastName() {
		return this.form.get("lastName");
	}

	get email() {
		return this.form.get("email");
	}

	get birthDate() {
		return this.form.get("birthDate");
	}

	get basicSalary() {
		return this.form.get("basicSalary");
	}

	get status() {
		return this.form.get("status");
	}

	get group() {
		return this.form.get("group");
	}

	get description() {
		return this.form.get("description");
	}

	back() {
		this.location.back();
	}

	onSubmit() {
		const username = this.username?.value;
		const firstName = this.firstName?.value;
		const lastName = this.lastName?.value;
		const email = this.email?.value;
		const birthDate = this.birthDate?.value;
		const basicSalary = this.basicSalary?.value;
		const status = this.status?.value;
		const group = this.group?.value;
		const description = this.group?.value;

		if (
			!username ||
			!firstName ||
			!lastName ||
			!email ||
			!birthDate ||
			!basicSalary ||
			!status ||
			!group ||
			!description
		) {
			return;
		}

		const data: Employee = {
			username,
			firstName,
			lastName,
			email,
			birthDate: new Date(birthDate),
			basicSalary: Number(basicSalary),
			status,
			group,
			description: new Date(description),
		} as Employee;
		this.employeeService.storeEmployee(data).subscribe({
			next: () => {
				this.isShowToast = true;
				this.toastMessage = "Employee added";
				setTimeout(() => {
					this.router.navigate(["employee"]);
				}, 1500);
			},
		});
	}

	toggleToast() {
		this.isShowToast = !this.isShowToast;
		this.router.navigate(["employee"]);
	}
}
